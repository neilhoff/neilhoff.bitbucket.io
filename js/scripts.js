const skills = [
  {
    name: 'JavaScript',
    ability: '90%'
  },
  {
    name: 'Vue.js',
    ability: '85%'
  },
  {
    name: 'Quasar Framework',
    ability: '90%'
  },
  {
    name: 'AWS',
    ability: '80%'
  },
  {
    name: 'Architect Serverless',
    ability: '75%'
  },
  {
    name: 'UI/UX Design',
    ability: '75%'
  },
  {
    name: 'Node.js',
    ability: '75%'
  },
  {
    name: 'Ruby on Rails',
    ability: '50%'
  },
  {
    name: 'Windows Server',
    ability: '85%'
  },
  {
    name: 'Linux Administration',
    ability: '70%'
  },
  {
    name: 'SAP Basis',
    ability: '90%'
  },
  {
    name: 'SharePoint Administration',
    ability: '90%'
  }
]

const projects = [
  {
    id: 'stellar-nexus',
    description: 'Stellar Nexus</a>',
    dates: '2023 to current',
    role: 'Creator',
    details: [
      'Designed and developed a template for connecting Quasar and Architect serverless frameworks',
      'Speeds up the development of Quasar front-ends that also need a serverless backend',
      'Open Source project on <a href="https://github.com/neilhoff/StellarNexus" target="_blank">Github</a>'
    ]
  },
  {
    id: 'rr-to',
    description: 'Print Rental Returns and Transfer Orders',
    dates: '2023 to current',
    role: 'Lead Full-Stack Developer and Project Manager',
    details: [
      'Designed and developed a front-end to facilitate the printing of RRs and TOs that includes part images',
      'Setup an Azure file share. This allows the business to maintain the images from Windows explorer',
      'Designed a printable page that works for any number of parts',
      'Worked with SAP business analyst and developer to configure SAP and setup web services',
      'The project was completed early and the business loves it'
    ]
  },
  {
    id: 'idea-gallery',
    description: 'Skyline Idea Gallery',
    dates: '2022 to current',
    role: 'Lead Full-Stack Developer',
    details: [
      '<a href="https://skyline.com/idea-gallery" target="_blank">https://skyline.com/idea-gallery</a>',
      'Researched and prototyped the Idea Gallery using Hubspot development tools',
      'The prototype was well received and saved Skyline $40K by building it in-house',
      'Designed and developed a favorites system for potential customers',
      'Setup sign-in for employees to authenticate with Azure AD using MSAL.js',
      'Designed API to connect SAP prices to images in the gallery'
    ]
  },
  {
    id: 'skyline-reports',
    description: 'Skyline Reports',
    dates: '2017 to current',
    role: 'Lead Full-Stack Developer',
    details: [
      'Designed and developed a website for displaying SAP reports',
      'Setup sign-in with Azure AD using MSAL.js',
      'Designed and developed a responsive front-end using Quasar (a Vue.js framework)',
      'Developed AWS Lambdas with the Architect serverless framework to securely connect to SAP APIs'
    ],
    images: [
      {
        src: 'img/skyline-reports/1-sign-in.png',
        alt: 'Sign-in Page',
        title: 'Sign-in Page',
        desc: ''
      },
      {
        src: 'img/skyline-reports/2-home.png',
        alt: 'Home Page',
        title: 'Home Page',
        desc: ''
      },
      {
        src: 'img/skyline-reports/3-home-dark-mode.png',
        alt: 'Dark Mode Home Page',
        title: 'Dark Mode Home Page',
        desc: ''
      },
      {
        src: 'img/skyline-reports/4-order-status.png',
        alt: 'Order Status Report',
        title: 'Order Status Report',
        desc: ''
      },
      {
        src: 'img/skyline-reports/5-custom-page-tracking.png',
        alt: 'Custom Page Tracking',
        title: 'Custom Page Tracking',
        desc: ''
      },
      {
        src: 'img/skyline-reports/6-custom-error-tracking.png',
        alt: 'Custom Error Tracking',
        title: 'Custom Error Tracking',
        desc: ''
      },
      {
        src: 'img/skyline-reports/7-dealer-ad-admin.png',
        alt: 'Dealer Active Directory Admin',
        title: 'Dealer Active Directory Admin',
        desc: ''
      }
    ]
  },
  {
    id: 'intranet',
    description: 'Skyline Intranet Redesign',
    dates: '2017 to 2018',
    role: 'Lead developer',
    details: [
      'Built 10 custom SharePoint web parts using the SharePoint Framework (SPFx – JavaScript based)'
    ]
  },
  {
    id: 'web-re-design',
    description: 'Skyline.com Redesign',
    dates: '2014 to 2016',
    role: 'Lead Full-Stack Developer for Skyline',
    details: [
      'Worked with a third party to build a custom CMS using Ruby on Rails'
    ]
  },
  {
    id: 'sharepoint-upgrade',
    description: 'Sharepoint 2013 Upgrade',
    dates: '2013 to 2014',
    role: 'Lead SharePoint Administrator',
    details: [
      'Researched, planned and lead the upgrade of SharePoint 2007 to SharePoint 2013',
      'Developed PowerShell scripts to automate manual upgrade tasks'
    ]
  },
  {
    id: 'sap-upgrade',
    description: 'SAP Upgrade',
    dates: '2010 to 2011',
    role: 'Lead SAP Basis Administrator',
    details: [
      'Researched, planned and lead the upgrade of all Skyline SAP systems',
      'Systems upgraded were: SAP ERP, SAP BI/BW, SAP Netweaver Portal, SAP TREX and SAP Content Server',
      'Partnered with ABAP developers to update custom code to work with the new versions'
    ]
  }
]

let projectsHtml = document.getElementById('projects')
for (const project of projects) {
  const mappedDetails = project.details.map( detail => `<li>${detail}</li>`)
  const projectDetails = mappedDetails.join('')
  const modalLinkClass = project.images ? 'project-link' : ''
  const onClick = project.images ? `onClick="openProjectModal('${project.id}')"` : ''
  const html = `
    <div class="project">
      <div class="project-description ${modalLinkClass}" ${onClick}>
        ${project.description} - ${project.dates}
      </div>
      <div class="project-role">${project.role}</div>
      <ul class="no-bullets">
        ${projectDetails}
      </ul>
    </div>
  `
  projectsHtml.innerHTML = projectsHtml.innerHTML + html
}

let skillsHtml = document.getElementById('skills')
for (const skill of skills) {
  const html = `
  <div class="skill">
    <div class="skill-name">${skill.name}</div>
    <div class="progress-container">
      <div class="progress">
        <span style="width:${skill.ability}"></span>
      </div>
    </div>
  </div>`
  skillsHtml.innerHTML = skillsHtml.innerHTML + html
}

function getMainImageHtml (image) {
  return {
    mainHtml: `<img alt="${image.alt}" class="project-main-image" src="${image.src}">`,
    titleHtml: `<h5>${image.title}</h5>`,
    descriptionHtml: `
      <div>${image.desc}</div>
    ` 
  }
}

function getProjectModalHtml (projectId) {
  const projectDetails = projects.find(p => p.id === projectId)
  const images = projectDetails.images
  const mainImageHtml = getMainImageHtml(images[0])
  
  const imageHtml = images.map( (image, i) => {
    const activeProject = i === 0 ? 'project-image-thumb-active' : ''
    return `<img alt="${image.alt}" class="project-image-thumb ${activeProject}" onClick="setAsMainImage('${projectId}', '${image.src}')" src="${image.src}">`
  }).join('')

  return `
    <div class="project-container">
      <header class="modal__header">
        <h4>
        ${projectDetails.description}
        </h4>
        <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
      </header>
      <div class="modal__subheader">${projectDetails.dates}</div>
      <div class="modal__content">
        <div id="project-image-title">
          ${mainImageHtml.titleHtml}
        </div>
        <div class="project-images">
          <div id="modal-main-image">
            ${mainImageHtml.mainHtml}
          </div>
          <div class="project-image-thumb-container">
            ${imageHtml}
          </div>
        </div>
        <div id="project-image-description">
          ${mainImageHtml.descriptionHtml}
        </div>
      </div>
    </div>
  `
}

function setAsMainImage (projectId, imageSrc) {
  // Set clicked item as active
  document.querySelectorAll('.project-image-thumb').forEach(e => {
    if(e.src.includes(imageSrc)){
      e.classList.add('project-image-thumb-active')
    } else {
      e.classList.remove('project-image-thumb-active')
    }
  })

  // Set the html
  const projectDetails = projects.find(p => p.id === projectId)
  const image = projectDetails.images.filter(image => image.src === imageSrc)
  const mainImageTitleDom = document.getElementById('project-image-title')
  const mainImageDom = document.getElementById('modal-main-image')
  const mainImageDescDom = document.getElementById('project-image-description')
  const imageHtml = getMainImageHtml(image[0])
  mainImageTitleDom.innerHTML = imageHtml.titleHtml
  mainImageDom.innerHTML = imageHtml.mainHtml
  mainImageDescDom.innerHTML = imageHtml.descriptionHtml
}

function openProjectModal (id) {
  let projectModal = document.getElementById('project-modal')
  const modalContainer = projectModal.getElementsByClassName('modal__container')
  modalContainer[0].innerHTML = getProjectModalHtml(id)
  MicroModal.show('project-modal')
}
